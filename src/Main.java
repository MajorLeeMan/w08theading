/*
This program will ask the user to hit ENTER when the prompt says GO
Like an old western quickdraw, the program will run two threads:
    thread one will execute main and wait for the user input
    thread two will count milliseconds until the user hits enter
The program will display a result showing how many milliseconds it took for the user to respond
The user can opt to play again, or exit the program.
*/

//imports
import java.io.IOException;
import java.util.Scanner;

public class Main extends Thread {

    public static void main(String[] args) {
        int r=0;
        while (r == 0) {
        //starting variables
        Jumpstart spool = new Jumpstart(); //create the thread
        Scanner keyboard = new Scanner(System.in);
        char input;
        int i=0;

        while (i == 0) {
                //if the user would like to play, press enter
                System.out.println("Wanna play DRAW? When I say DRAW, press enter! (y/n)");
                input = keyboard.next().charAt(0); //accept the first character
                //System.out.println(input);

                //based on user input, begin the game or exit
                switch (input) {
                    case 'n', 'N' -> {
                        System.out.println("Bye.");
                        r=1;
                        System.exit(0);
                    }
                    case 'y', 'Y' -> i = 1; //break the loop
                    default -> {
                        i = 0;
                        System.out.println("Not a valid response.");
                    }
                }
            } //exit the while loop of initial inquiry

                System.out.println("Ready?...");
                //apply a random number to the sleep time so user cannot predict when DRAW appears
                double bumble = (Math.random());
                long humble = Math.round(((bumble + 1) * 1000) / 2);
                //System.out.println(humble); //display the delay for debugging
                try {
                    sleep(humble);
                } catch (InterruptedException d) {
                }


                //begin countdown:
                spool.start(); //interrupt if user hits enter
                System.out.println("DRAW!"); //display DRAW

                hitEnter(); //once the user hits enter,
                spool.interrupt(); //interrupt
                //System.out.println("End of Spool ideally.");


                if (spool.time <= 0) {
                    //if user has a 0 value (premature Enter), tell them they were too early
                    System.out.println("You were too quick on the draw, and shot " +
                            "a man with his gun in the holster.");
                }
                if (spool.time >= 1000) {
                    //if user has 1000, they were too late
                    System.out.println("You were too slow on the draw, and defeated " +
                            "by Slug-eye Sam.");
                }
                if (spool.time > 0 && spool.time < 1000) {
                    //if user has a valid value (between 0 and 1000, display it
                    System.out.println("Your reaction time was " + spool.time +
                            " milliseconds. Nice!");
                }
            }
        }

    public static void hitEnter(){
        //read the value (which won't be used) when enter is hit, which continues the code
        try {int read = System.in.read(new byte[2]);} catch (IOException i) {};


    }


}

