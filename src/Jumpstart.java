import java.util.InputMismatchException;

public class Jumpstart extends Thread {
    //object variables for this thread
    boolean counting = true;
    int time=0;

    public void run() {
        //run a new thread
        time=0;
        //System.out.println("Is this the start of a real thread?");
        while (counting) {
            time++;
            if (time>=5000) {counting=false;}
            //System.out.println(time);
            try {sleep(1);} catch (InterruptedException a) {
                counting=false;
                }

            }
        //System.out.println("End of run() method:"+time);
        }

    }
